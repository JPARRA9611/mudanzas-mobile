import React from 'react';
import { StyleSheet, Text, View, TextInput, Image, Alert } from 'react-native';
import { Icon,Avatar,Header,Button, SocialIcon } from 'react-native-elements';

export default class Login extends React.Component{

  state={
    usuario:'',
    password:''
  }

  validarCampos=()=>{
    if(this.state.usuario=='' || this.state.password==''){
      this.notificacion('Inicio de sesion','Los campos no pueden estar vacios');
      return false;
    }
    this.iniciarSesion();
  }

  notificacion=(titulo,texto)=>{
    Alert.alert(
      titulo,
      texto,
      [
        {text:"Aceptar",onPress:()=>console.log('Ok')}
      ]
    );
  }

  iniciarSesion=()=>{
    this.props.navigation.navigate('Menu');
  }

  render(){
    return (
      <View style={styles.container}>
        <Text>Mudanzas chico</Text>
        <Image style={{width:'100%',height:200}} source={require('./assets/logoMudanzas.png')} />
        <View style={{flex:1,flexDirection:'row'}}>
          <SocialIcon
            type='twitter'
            style={{width:50,height:50}}
          />

          <SocialIcon
            type='facebook'
            style={{width:50,height:50}}
          />

          <SocialIcon
            type='linkedin'
            style={{width:50,height:50}}
          />
        </View>
        <View style={{marginTop:40}}>
          <TextInput
            style={styles.inputLogin}
            placeholder="Usuario"
            autoFocus={true}
            onChangeText={(text)=>this.setState({usuario:text})}
          />
          <TextInput
            style={styles.inputLogin}
            placeholder="Contraseña"
            onChangeText={(text)=>this.setState({password:text})}
            password={true}
            secureTextEntry={true}
          /> 
          <View style={{marginTop:30}}>
            <Button
              title="Iniciar Sesion"
              type="solid"
              onPress={this.validarCampos.bind()}
              
            />
          </View>

          <View style={{marginTop:30,textAlign:'center'}}>
            <Button 
              title="Volver"
              buttonStyle={{backgroundColor:'#e74c3c'}}
              onPress={()=>this.props.navigation.navigate('Inicio')}
            />
        </View>
          
        </View>
      </View>
    )
  } 
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  inputLogin: {
    width:300,
    height: 40,
    marginTop:30,
    borderColor:'#2980b9',
    borderWidth:1,
    padding:10,
    borderRadius:5
  }
});