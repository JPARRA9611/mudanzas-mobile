import React from 'react';
import { View,StyleSheet,Text,Image } from 'react-native';
import FlashMessage,{ showMessage, hideMessage } from "react-native-flash-message";


export default class BoxPrice extends React.Component{

    constructor(props){
        super(props);
    }

    mensaje=()=>{
        showMessage({
            message: "Hello World",
            description: "This is our second message",
            type: "success",
            backgroundColor:'blue'
          });
    }

    render(){
        const { navigation } = this.props.props;
        return(                 
            <View style={styles.containerView} >

                <FlashMessage position="bottom" />
                <View style={{height:'20%',borderBottomWidth:1,borderColor:'#ecf0f1',flex:1,justifyContent:'center'}}>
                    <Text style={{fontSize:20,paddingTop:20,paddingLeft:20}}>{this.props.titulo}</Text>
                </View>
                <View style={{height:'60%'}}>
                    <Image style={{width:'100%',height:'80%',paddingLeft:20}} source={require('./assets/logoMudanzas.png')} onPress={this.mensaje}/>
                    <Text style={{height:'10%',paddingLeft:20}}>{this.props.descripcion}</Text>
                    <Text style={{height:'10%',paddingLeft:20}}>${this.props.precio}</Text>
                </View>
                <View style={{height:'20%',borderTopWidth:1,borderColor:'#ecf0f1',flex:1,justifyContent:'center'}}>
                    <Text onPress={() =>
                                        navigation.push('Detalle', {
                                            itemId: Math.floor(Math.random() * 100),
                                            type:2
                                        })} style={{fontSize:20,paddingLeft:20}}>{this.props.titulo}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerView:
        {
            width:'90%',
            marginTop:30,
            height:400,
            marginLeft:20,
            marginRight:10,
            borderColor:'#ecf0f1',
            elevation:4,
            marginBottom:30
        }
  });