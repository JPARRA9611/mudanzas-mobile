import React from 'react';
import {Cache} from 'react-native-cache';




    export const setCache=(key,value)=>{
        const cache = new Cache({
            namespace:'mudanzas-mobile',
            policy:{
                maxEntries: 50000
            },
            
        });

        cache.setItem(key,value,function(err){

        });
        return true;
    }

    export const getCache=(key)=>{
        const cache = new Cache({
            namespace:'mudanzas-mobile',
            policy:{
                maxEntries: 50000
            }            
        });
        var val;

        cache.getItem(key,function(err,value){
            val=value;
        });
        return val;
    }

    export const deleteCache=(key)=>{
        const cache = new Cache({
            namespace:'mudanzas-mobile',
            policy:{
                maxEntries: 50000
            }            
        });
        cache.removeItem(key, function(err) {
            
        });
    }
