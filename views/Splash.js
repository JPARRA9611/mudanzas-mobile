import React from 'react';
import { View,StyleSheet,Text,Image} from 'react-native';
import * as Font from 'expo-font';
import { Button,Icon } from 'react-native-elements';
import AppIntroSlider from 'react-native-app-intro-slider';


export default class Presentacion extends React.Component{
    static navigationOptions={
        header: null,
    }

    state = {
        fontLoaded: false,
        showRealApp:false,
        slides:[
            {
                key:'1',
                title:'App haz tu mudanza',
                text:'Con esta App nadie te hace la mudanza, te alquilamos las herramientas para que hagas tu mismo la mudanza',
                image:require('../assets/app.png'),
                backgroundColor:'#B4C5F0',
                type:1
            },
            {
                key:'2',
                title:'Herramientas',
                text:'Esta App te alquila las herramientas para que hagas tu mudanza. Conoce las herramientas: Vehiculos, materiales y operarios',
                image:require('../assets/tools.png'),
                backgroundColor:'#5599FE',
                type:2
            },
            {
                key:'3',
                title:'Mudanza',
                text:'Al escojer las herramientas y materiales programa la fecha en el calendario para realizar tu mismo la mudanza',
                image:require('../assets/calendar.png'),
                backgroundColor:'#8EA5AC',
                type:3
            }
        ]
    };

    async componentDidMount() {
        await Font.loadAsync({
          'open-sans-condensed': require('../assets/fonts/OpenSansCondensed-Light.ttf'),
        });
        this.setState({ fontLoaded: true });
    }

    _renderItem=(item)=>{
        const { navigation } = this.props;
        var item=item.item
        return (
            <View style={{backgroundColor:item.backgroundColor,width:'100%',height:'100%',flex:1,alignItems:'center'}}>
                <Text style={{marginTop:50,color:'white',fontSize:50,fontFamily:'open-sans-condensed'}}>{item.title}</Text>
                <Image style={{marginTop:10,width:'100%',height:'30%'}} source={item.image}/>
                <Text style={{marginTop:10,color:'white',fontSize:40,fontFamily:'open-sans-condensed',paddingLeft:10}}> {item.text}</Text>
                <Button
                    title="Saber Mas"
                    raised
                    containerStyle={{marginTop:30,width:'90%'}}
                    onPress={() =>
                        navigation.push('Presentacion',{
                            type:item.type
                        })}
                />
            </View>
        )
    }

    _renderNextButton=()=>{
        return(
            <View style={{width:40,height:40,backgroundColor:'rgba(0,0,0,.2)',borderRadius:20,justifyContent:'center',alignItems:'center'}}>
                <Icon
                    raised
                    name="arrow-right"
                    type="font-awesome"
                    color="#3498db"
                />
            </View>
        )
    }

    _renderDoneButton=()=>{
        return(
            <View style={{width:90,height:40,backgroundColor:'#3498db',borderRadius:20,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'white'}}>Siguiente</Text>
            </View>
        )
    }

    _onDone=()=>{
        const { navigation } = this.props;
        navigation.push('Calendar')
        this.setState({showRealApp:true});
    }

    render(){
        return(
            this.state.fontLoaded ? (
                <AppIntroSlider 
                    renderItem={this._renderItem} 
                    slides={this.state.slides} 
                    onDone={this._onDone}
                    renderNextButton={this._renderNextButton}
                    renderDoneButton={this._renderDoneButton}
                />
            ) : null
        )
    }
}

const styles=StyleSheet.create({
    buttonCircle:{
        width:40,
        height:40,

    }
})