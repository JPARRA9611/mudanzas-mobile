import React from 'react';
import { View,StyleSheet,Text,Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Button } from 'react-native-elements';



export default class OtrosProductos extends React.Component{


    render(){
        return(
            <View>
                <ScrollView>
                    <View style={{paddingHorizontal:20,paddingVertical:20}}>
                        <View style={{borderWidth:1,paddingHorizontal:10,paddingVertical:10,borderColor:'#ecf0f1',elevation:4,}}>
                            <Text style={{fontSize:20,borderBottomWidth:1,borderColor:'#bdc3c7'}}>Otros productos que te pueden interesar</Text>
                            <View>
                                <View style={{flex:1,flexDirection:'column'}}>
                                    <View style={{width:'100%',flex:1,flexDirection:'row',marginBottom:10,marginTop:5}}>
                                        <View style={{width:'40%'}}>
                                            <Image style={{width:'100%',height:'100%'}} source={require('../assets/caja.jpg')}/>
                                        </View>
                                        <View style={{width:'60%'}}>
                                            <View style={{paddingLeft:5,flex: 1,justifyContent: 'flex-end',marginTop:20}}>
                                                <Text style={{fontSize:20,fontWeight:'bold',marginBottom:10}}>$60.000</Text>
                                                <Text style={{fontSize:15,marginBottom:20}}>Plastico de burbujas</Text>
                                                <Button
                                                    title="Ver Detalle"
                                                />
                                            </View>
                                        </View>
                                    </View>

                                    <View style={{width:'100%',flex:1,flexDirection:'row',marginBottom:10,marginTop:5}}>
                                        <View style={{width:'40%'}}>
                                            <Image style={{width:'100%',height:'100%'}} source={require('../assets/caja.jpg')}/>
                                        </View>
                                        <View style={{width:'60%'}}>
                                            <View style={{paddingLeft:5,flex: 1,justifyContent: 'flex-end',marginTop:20}}>
                                                <Text style={{fontSize:20,fontWeight:'bold',marginBottom:10}}>$60.000</Text>
                                                <Text style={{fontSize:15,marginBottom:20}}>Plastico de burbujas</Text>
                                                <Button
                                                    title="Ver Detalle"
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}