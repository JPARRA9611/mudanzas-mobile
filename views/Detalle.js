import React from 'react';
import { StyleSheet, Text, View,ScrollView,Image,FlatList } from 'react-native';
import InformacionVehiculos from '../views/InformacionVehiculos';
import InformacionEmbalaje from '../views/InformacionEmbalaje';
import { Icon, Button } from 'react-native-elements';

export default class Detalle extends React.Component{
    
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: (
                <View style={{flex:1,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                    <View>
                        <Text style={{color:'white',fontSize:15,fontWeight:'bold'}}>Mudanzas Chico</Text>
                    </View>
                </View>
            ),
            headerRight: (
                <View style={{padding:10}}>
                    <Icon
                        name='shopping-cart'
                        type='font-awesome'
                        color='white'
                        onPress={()=>navigation.navigate('Cart')}
                    />
                </View>
            ),
        };
    };

    constructor(props){
        super(props);
        this.state={
            type:this.props.navigation.state.params.type
        }
    }

    

    render(){
        const descripcion=[
            {key: 'Peso de la carga: Hasta 4.5 Toneladas'},
            {key: 'Alto: Entre 2.20 y 2.30 metros'},
            {key:'Largo: Entre 4 y 5 metros'},
            {key:'Ancho: Entre 2.20 y 2.30 metros'},
            {key:'Cubicaje: Entre 22 y 23 metros cúbicos'}
        ];

        const imagenes=[
            {
              title: 'Title 1',
              caption: 'Caption 1',
              url: '../assets/logoMudanzas.png',
            },
            {
                title: 'Title 2',
                caption: 'Caption 2',
                url: '../assets/logoMudanzas.png',
            },
            {
                title: 'Title 3',
                caption: 'Caption 3',
                url: '../assets/logoMudanzas.png',
            },
        ];
        var renderDetalle;
        if(this.state.type==1){
            renderDetalle=<InformacionVehiculos
                titulo="Turbo"
                precio="60.000"
                descripcion={descripcion}
                imagenes={imagenes}
            />
            
        }else if(this.state.type==2){
            renderDetalle=<InformacionEmbalaje
                titulo="Embalaje"
                precio="60.000"
                descripcion={descripcion}
                imagenes={imagenes}
            />
        }
        
        
        return(
            <View>
                {renderDetalle}
            </View>
        )
    }
}