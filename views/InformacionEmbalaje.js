import React from 'react';
import { StyleSheet, Text, View,ScrollView,Image,FlatList } from 'react-native';
import { Icon, Button } from 'react-native-elements';
import Slideshow from 'react-native-image-slider-show';
import FlashMessage,{ showMessage, hideMessage} from 'react-native-flash-message';
import {setCache,getCache,deleteCache} from "../CacheApp";

export default class InformacionEmbalaje extends React.Component{
    
    
    constructor(props) {
        super(props);
     
        this.state = {
            position: 1,
            interval: null,
            dataSource: this.props.imagenes,
        };
    }

    componentWillMount() {
        this.setState({
          interval: setInterval(() => {
            this.setState({
              position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
            });
          }, 2000)
        });
    }
     
    componentWillUnmount() {
        clearInterval(this.state.interval);
    }

    addCart=(precio,titulo)=>{
        var productos=getCache('productos');
        var productoNuevo={
            descripcion:titulo,
            valor:precio,
            url:'../doble-troque.png'
        };
        productos.push(productoNuevo);
        deleteCache('productos');
        setCache('productos',productos);
        showMessage({
            message: ""+titulo+" Agregado satisfactoriamente",
            type: "success",
        });
    }

    render(){
        
        return(
            <View>
                <ScrollView>
                    <View>
                        <View style={{height:200,width:'100%'}}>
                                
                            <Slideshow 
                                
                                titleStyle={{color:'white',fontSize:30}}
                                captionStyle={{color:'white',fontSize:20}}
                                dataSource={this.state.dataSource}
                                position={this.state.position}
                                onPositionChanged={position => this.setState({ position })} 
                            />
                        </View>
                        <View style={{width:'100%',height:150,backgroundColor:'#ecf0f1',paddingLeft:30,borderTopWidth:1}}>
                            <Text style={{marginTop:30,fontSize:30}}>{this.props.titulo}</Text>
                            <Text style={{fontSize:30}}>${this.props.precio}</Text>
                        </View>
                        <View style={{width:'100%',height:300,backgroundColor:'white',paddingLeft:30,marginBottom:20}}>
                            <Text style={{fontSize:30}}>Descripción</Text>
                            <FlatList
                                data={this.props.descripcion}
                            renderItem={({item}) => <Text style={{fontSize:15}}>{item.key}</Text>}
                            />
                            <Text style={{fontSize:30}}>Sugerencias</Text>
                            <Text style={{fontSize:15}}>Este tipo de vehiculo se sugiere para</Text>
                        </View>
                        
                        <View>
                        <Button
                            icon={
                                <Icon
                                name="spa"
                                size={15}
                                color="white"
                                />
                            }
                            title="Agregar al carrito"
                            onPress={()=>this.addCart(this.props.precio,this.props.titulo)}
                            />
                        </View>
                    </View>
                    <FlashMessage position="bottom" />
                    
                </ScrollView>
                
            </View>
        )
    }
}