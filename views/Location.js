import React, { Component } from "react";
import { Button, View,Alert } from "react-native";
import {setCache} from "../CacheApp";
import MapView,{Marker} from 'react-native-maps'
import {Input} from 'react-native-elements'

export default class Location extends Component {
    static navigationOptions={
        header: null,
    }

    constructor(props) {
      super(props);
      this.state = {
          region:null,
          latitude:4.5802318,
          longitude:-74.1438553,
          direccion:'',
          tipo:'',
          nombre:'',
          cedula:'',
      };
      
    }

    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            position=>{
                const location = JSON.stringify(position);
                this.setState({location:location});
                this.setState({latitude:position.coords.latitude});
                this.setState({longitude:position.coords.longitude});  
                this.setState({markers:{
                    title:'mi Ubicacion',
                    description:'Esta es mi ubicación',
                    coordinate:[this.state.latitude,this.state.longitude]
                }});
            },
            error=> Alert.alert(error.message),
            {enableHighAccuaracy:true,timeout:20000,maximumAge:1000}
        ) 
    }

    saveLocation=()=>{
        setCache('direccion',this.state.direccion);
        setCache('tipoVivienda',this.state.tipo);
        setCache('nombre',this.state.nombre);
        setCache('identificacion',this.state.cedula);
        const { navigation } = this.props;
        navigation.push('Inicio');
    }

    render() {
        const { navigation } = this.props;
        return (
            <>
                <View style={{flex:1}}>
                    {this.state.location ? (
                    
                    <MapView
                        style={{flex:1}}
                        region={{
                            latitude:this.state.latitude,
                            longitude:this.state.longitude,
                            latitudeDelta:0.0143,
                            longitudeDelta:0.0134
                        }}
                        showsUserLocation
                        loadingEnabled
                    >   
                        <Marker
                            coordinate={{latitude:this.state.latitude,longitude:this.state.longitude}}
                            title="mi Ubicacion"
                            description='Esta es mi ubicación'
                        />   
                    </MapView>) : null}
                    
                </View>

                <View style={{flex:1}}>
                    <View style={{flex:1,alignItems:'center',marginTop:25}}>
                        <Input
                            placeholder="Escriba su Nombre"
                            rightIcon={{type:'font-awesome',name:'user'}}
                            errorStyle={{ color: 'red' }}
                            errorMessage='Este Campo es obligatorio'
                            onChange={(e)=>{this.setState({'nombre':e.nativeEvent.text})}}
                        />

                        <Input
                            placeholder="Escriba su Cedula"
                            rightIcon={{type:'font-awesome',name:'id-card'}}
                            errorStyle={{ color: 'red' }}
                            errorMessage='Este Campo es obligatorio'
                            onChange={(e)=>{this.setState({'cedula':e.nativeEvent.text})}}
                        />

                        <Input
                            placeholder="Escriba su dirección"
                            rightIcon={{type:'font-awesome',name:'map-marker'}}
                            errorStyle={{ color: 'red' }}
                            errorMessage='Este Campo es obligatorio'
                            onChange={(e)=>{this.setState({'direccion':e.nativeEvent.text})}}
                        />

                        <Input
                            placeholder="Especifique si es casa o apartamento"
                            rightIcon={{type:'font-awesome',name:'building'}}
                            errorStyle={{ color: 'red' }}
                            errorMessage='Este Campo es obligatorio'
                            onChange={(e)=>{this.setState({'tipo':e.nativeEvent.text})}}
                        />

                        {this.state.direccion!=='' && this.state.tipo!=='' && this.state.cedula!=='' && this.state.nombre!=='' ? (
                        <View style={{marginTop:50}}>
                            <Button
                                title="Aceptar"
                                type="outline"
                                onPress={this.saveLocation}
                            />
                        </View>
                        ) : null}
                    </View>

                    
                </View>
            </>
        );
    }
}