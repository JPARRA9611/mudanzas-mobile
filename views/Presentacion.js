import React from 'react';
import { View,StyleSheet,Text,Image,ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Button } from 'react-native-elements';
import FlashMessage,{ showMessage, hideMessage } from "react-native-flash-message";
import FadeInView from '../Animations';
import InformacionApp from '../views/InformacionApp';
import InformacionHerramienta from '../views/InformacionHerramienta';
import InformacionMudanza from '../views/InformacionMudanza';




export default class Presentacion extends React.Component{
    static navigationOptions={
        title:'Mudanzas Chico'
    }
    constructor(props) {
        super(props);
        this.state={
            type:this.props.navigation.state.params.type
        }
    }

    mensaje=()=>{
        showMessage({
            message: "Hello World",
            description: "La idea es que tu realizes tu mudanzas",
            type: "success",
          });
    }
      

    render(){
        const { navigation } = this.props;
        if(this.state.type==1){
            renderDetalle=<InformacionApp
                navigation={navigation}
            />
        }else if(this.state.type==2){
            renderDetalle=<InformacionHerramienta/>
        }else if(this.state.type==3){
            renderDetalle=<InformacionMudanza/>
        }
        return(
            <View>
                {renderDetalle}
            </View>    
        )
    }
}