import React from 'react';
import { View,StyleSheet,Text,Image,ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Button } from 'react-native-elements';
import FlashMessage,{ showMessage, hideMessage } from "react-native-flash-message";
import FadeInView from '../Animations';




export default class InformacionHerramienta extends React.Component{
    static navigationOptions={
        title:'Mudanzas Chico'
    }
    constructor(props) {
        super(props);
        this.state={
            vehiculos:[
                {
                    nombre:'Carry',
                    cgf:'2 Ejes',
                    peso:'300 KG',
                    pesoC:'1.80 Toneladas',
                    largo:'1.5 metros',
                    alto:'1.20 metros',
                    volumen:'2.16 M3',
                    ancho:'1.20 metros',
                    imagen:require('../assets/vehiculo-turbo.png')
                },
                {
                    nombre:'NHR',
                    cgf:'2 Ejes',
                    peso:'1.50 Toneladas',
                    pesoC:'3.80 Toneladas',
                    largo:'3 metros',
                    alto:'2.10 metros',
                    volumen:'11.34 M3',
                    ancho:'1.80 metros',
                    imagen:require('../assets/vehiculo-turbo.png')
                },
                {
                    nombre:'Turbo',
                    cgf:'2 Ejes',
                    peso:'4.5 Toneladas',
                    pesoC:'9 Toneladas',
                    largo:'4.80 metros',
                    alto:'2.20 metros',
                    volumen:'23.23 M3',
                    ancho:'2.20 metros',
                    imagen:require('../assets/vehiculo-turbo.png')
                },
                {
                    nombre:'Sencillo',
                    cgf:'2 Ejes',
                    peso:'8.5 Toneladas',
                    pesoC:'16 Toneladas',
                    largo:'6.50 metros',
                    alto:'2.15 metros',
                    volumen:'33.54 M3',
                    ancho:'2.40 metros',
                    imagen:require('../assets/camion-sencillo.png')
                }
            ]
        }
    }

    mensaje=()=>{
        showMessage({
            message: "Hello World",
            description: "La idea es que tu realizes tu mudanzas",
            type: "success",
          });
    }
      

    render(){
        const { navigation } = this.props;
        const vehiculos=this.state.vehiculos.map((val,k)=>{
            return(
                <FadeInView style={styles.viewEnunciado} key={k}>
                    <Image style={{backgroundColor:'rgba(236, 240, 241,.2)'}} source={val.imagen} />
                    <Text style={styles.textEnunciado}>{val.nombre}</Text>
                    <Text style={styles.textEnunciado}>CGF: {val.cgf}</Text>
                    <Text style={styles.textEnunciado}>Peso con carga {val.pesoC}</Text>
                    <Text style={styles.textEnunciado}>Peso de la carga: {val.peso}</Text>
                    <Text style={styles.textEnunciado}>Alto: {val.alto}</Text>
                    <Text style={styles.textEnunciado}>Largo: {val.largo}</Text>
                    <Text style={styles.textEnunciado}>Ancho: {val.ancho}</Text>
                    <Text style={styles.textEnunciado}>Volumen: {val.volumen}</Text>
                    {/*<View style={styles.marginTop30}>
                        <Button
                            title="Ver mas"
                            onPress={() =>
                                navigation.push('Inicio')}
                        />
                    </View>*/}
                </FadeInView>
            )
        });

        return(
            <View>
                <ScrollView>
                    <ImageBackground source={require('../assets/landing.jpg')} style={[styles.height100,styles.width100]}>
                        <View style={[styles.height100,styles.width100,styles.padding20,styles.backgroundWhite]}>
                            <View style={{borderWidth:5,width:'100%',height:'100%',borderColor:'#3498db'}}>
                                <View style={{flex: 1,alignItems: 'center',flexDirection: 'column',paddingHorizontal:20,paddingVertical:20}}>
                                    <View style={styles.backgroundWhite}>
                                        <Image source={require('../assets/logoMudanzas.png')} />
                                    </View>
                                    
                                    {vehiculos}
                                </View>
                            </View>                    
                        </View>
                    </ImageBackground>
                </ScrollView>
                <FlashMessage position="bottom" />
            </View>    
        )
    }
}

const styles = StyleSheet.create({
    textEnunciado:
        {
            fontSize:20,
            color:'#ecf0f1'
        },
    viewEnunciado:
        {
            marginTop:30,
            backgroundColor:'rgba(52, 73, 94,0.4)',
            paddingHorizontal:20,
            paddingVertical:20,
            textAlign:'center'
        },
    marginTop30:
        {
            marginTop:30
        },
    width100:
        {
            width:'100%'
        },
    height:
        {
            height:'100%'
        },
    padding20:
        {
            paddingHorizontal:20,
            paddingVertical:20
        },
    backgroundWhite:
        {
            backgroundColor: 'rgba(236, 240, 241, 0.4)'
        }
  });