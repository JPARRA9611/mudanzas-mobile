import React, { Component } from "react";
import { Button, View,Text,Image } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import * as Font from 'expo-font';
import {setCache} from "../CacheApp";

export default class Calendar extends Component {
    static navigationOptions={
        header: null,
    }

    constructor(props) {
      super(props);
      this.state = {
        fontLoaded: false,
        isDatePickerVisible: false,
        isTimePickerVisible: false,
        fecha:'0000-00-00',
        hora:'00:00'
      };
      
    }

    async componentDidMount() {
        
        await Font.loadAsync({
          'orbitron': require('../assets/fonts/Orbitron-Regular.ttf'),
        });
        this.setState({ fontLoaded: true });
    }
  
    showDatePicker = () => {
      this.setState({ isDatePickerVisible: true });
    };
  
    hideDatePicker = () => {
      this.setState({ isDatePickerVisible: false });
    };
  
    handleDatePicked = date => {
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        let fecha=year + '-' + (month+1) + '-' + day;
        this.setState({fecha:fecha});
        this.hideDatePicker();
    };

    showTimePicker = () => {
        this.setState({ isTimePickerVisible: true });
    };
    
    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false });
    };
    
    handleTimePicked = date => {
        var horas = date.getHours();
        var minutos = date.getMinutes();
        var segundos = date.getSeconds();
        let hora=horas + ':' + minutos + ':' + segundos+'0';
        this.setState({hora:hora});
        this.hideTimePicker();
    };
    
    saveDate = () =>{
        

        setCache('fecha',this.state.fecha);

        setCache('hora',this.state.hora);

        const { navigation } = this.props;
        navigation.push('Location');
    }

    render() {
        return (
            <>
                <View style={{flex:1,backgroundColor:'#8EA5AC',justifyContent:'center',alignItems:'center',padding:3}}>
                    <Image style={{width:'80%',height:'80%'}} source={require('../assets/calendar.png')}></Image>
                    <Text style={{color:'white',fontSize:20}}>Empezemos seleccionando la hora y fecha de tu mudanza</Text>
                </View>
                <View style={{flex:1,justifyContent:'center',alignItems:'center',flexDirection:'row',backgroundColor:'#ecf0f1'}}>
                    <View style={{backgroundColor:'#2980b9',flex:1,alignItems:'center',margin:3,padding:5}}>
                        {this.state.fontLoaded ? (
                            <Text style={{fontFamily:'orbitron',color:'white',fontSize: 15}}>{this.state.fecha}</Text>
                        ) : null}
                    </View>
                    <View style={{backgroundColor:'#2980b9',flex:1,alignItems:'center',margin:3,padding:5}}>
                        {this.state.fontLoaded ? (
                            <Text style={{fontFamily:'orbitron',color:'white',fontSize: 15}}>{this.state.hora}</Text>
                        ) : null}
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row',borderWidth:1,borderColor:'#8EA5AC',backgroundColor:'#8EA5AC'}}>
                    
                    <View style={{flex:1,justifyContent:'center',alignItems:'center',width:'50%'}}>
                        <Button title="Seleccionar Fecha" onPress={this.showDatePicker} /> 
                    </View>

                    <View style={{flex:1,justifyContent:'center',alignItems:'center',width:'50%'}}>
                        <Button title="Seleccionar Hora" onPress={this.showTimePicker} />

                    </View>
                            
                </View>
                    
                <View>
                    {this.state.fecha!=='0000-00-00' && this.state.hora!=='00:00' ? (
                        <Button title="Aceptar" onPress={this.saveDate} />
                    ) : null}
                </View>
            
                <DateTimePicker
                    isVisible={this.state.isDatePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDatePicker}
                    minimumDate={new Date()}
                    
                />

                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this.handleTimePicked}
                    onCancel={this.hideTimePicker}
                    minimumDate={new Date()}
                    mode="time"
                    is24Hour={false}
                />
            </>
        );
    }
  }