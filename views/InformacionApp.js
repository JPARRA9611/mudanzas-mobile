import React from 'react';
import { View,StyleSheet,Text,Image,ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Button } from 'react-native-elements';
import FlashMessage,{ showMessage, hideMessage } from "react-native-flash-message";
import FadeInView from '../Animations';




export default class InformacionApp extends React.Component{
    static navigationOptions={
        title:'Mudanzas Chico'
    }
    constructor(props) {
        super(props);
    }

    mensaje=()=>{
        showMessage({
            message: "Hello World",
            description: "La idea es que tu realizes tu mudanzas",
            type: "success",
          });
    }
      

    render(){
        const { navigation } = this.props.navigation;
        return(
            <View>
                <ScrollView>
                    <ImageBackground source={require('../assets/landing.jpg')} style={[styles.height100,styles.width100]}>
                        <View style={[styles.height100,styles.width100,styles.padding20,styles.backgroundWhite]}>
                            <View style={{borderWidth:5,width:'100%',height:'100%',borderColor:'#3498db'}}>
                                <View style={{flex: 1,alignItems: 'center',flexDirection: 'column',paddingHorizontal:20,paddingVertical:20}}>
                                    <View style={styles.backgroundWhite}>
                                        <Image source={require('../assets/logoMudanzas.png')} />
                                    </View>
                                    <FadeInView style={styles.viewEnunciado}>
                                        <Text style={styles.textEnunciado}>Bienvenido a Mudanzas Chico.</Text>
                                        <Text style={styles.textEnunciado}>Con esta App podras hacer tu mismo la mudanza alquilando las herramientas que necesites.</Text>
                                        {/*<View style={styles.marginTop30}>
                                            <Button
                                                title="Ver mas"
                                                onPress={() =>
                                                    navigation.push('Inicio')}
                                            />
                                        </View>*/}
                                    </FadeInView>

                                    <FadeInView style={styles.viewEnunciado}>
                                        <Text style={styles.textEnunciado}>Las herramientas a alquilar son: Vehiculos según tamaño, elementos de empaque y los operarios que consideres que necesitas.</Text>
                                        <Text style={styles.textEnunciado}>!!Es muy sencillo¡¡</Text>
                                        {/*<View style={styles.marginTop30}>
                                            <Button
                                                title="Ver mas"
                                                onPress={() =>
                                                    navigation.push('Inicio')}
                                            />
                                        </View>*/}
                                    </FadeInView>

                                    <FadeInView style={styles.viewEnunciado}>
                                        <Text style={styles.textEnunciado}>Los vehiculos te los alquilamos con conductor. El tamaño lo decides tu según la cantidad de cosas que componen tu mudanza</Text>
                                        {/*<View style={styles.marginTop30}>
                                            <Button
                                                title="Ver mas"
                                                onPress={() =>
                                                    navigation.push('Inicio')}
                                            />
                                        </View>*/}
                                    </FadeInView>

                                    <FadeInView style={styles.viewEnunciado}>
                                        <Text style={styles.textEnunciado}>Según el tamaño del vehiculo, tú decides cuantos operarios necesitas y la cantidad de empaque para que todo te salga bien</Text>
                                        <Text style={styles.textEnunciado}></Text>
                                        <Text style={styles.textEnunciado}></Text>
                                        {/*<View style={styles.marginTop30}>
                                            <Button
                                                title="Ver mas"
                                                onPress={() =>
                                                    navigation.push('Inicio')}
                                            />
                                        </View>*/}
                                    </FadeInView>
                                    
                                </View>
                            </View>                    
                        </View>
                    </ImageBackground>
                </ScrollView>
                <FlashMessage position="bottom" />
            </View>    
        )
    }
}

const styles = StyleSheet.create({
    textEnunciado:
        {
            fontSize:20,
            color:'#ecf0f1'
        },
    viewEnunciado:
        {
            marginTop:30,
            backgroundColor:'rgba(52, 73, 94,0.4)',
            paddingHorizontal:20,
            paddingVertical:20,
            textAlign:'center'
        },
    marginTop30:
        {
            marginTop:30
        },
    width100:
        {
            width:'100%'
        },
    height:
        {
            height:'100%'
        },
    padding20:
        {
            paddingHorizontal:20,
            paddingVertical:20
        },
    backgroundWhite:
        {
            backgroundColor: 'rgba(236, 240, 241, 0.4)'
        }
  });