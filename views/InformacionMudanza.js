import React from 'react';
import { View,StyleSheet,Text,Image,ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Button } from 'react-native-elements';
import FlashMessage,{ showMessage, hideMessage } from "react-native-flash-message";
import FadeInView from '../Animations';




export default class InformacionMudanza extends React.Component{
    static navigationOptions={
        title:'Mudanzas Chico'
    }
    constructor(props) {
        super(props);
    }

    mensaje=()=>{
        showMessage({
            message: "Hello World",
            description: "La idea es que tu realizes tu mudanzas",
            type: "success",
          });
    }
      

    render(){
        const { navigation } = this.props;
        return(
            <View>
                <ScrollView>
                    <ImageBackground source={require('../assets/landing.jpg')} style={[styles.height100,styles.width100]}>
                        <View style={[styles.height100,styles.width100,styles.padding20,styles.backgroundWhite]}>
                            <View style={{borderWidth:5,width:'100%',height:'100%',borderColor:'#3498db'}}>
                                <View style={{flex: 1,alignItems: 'center',flexDirection: 'column',paddingHorizontal:20,paddingVertical:20}}>
                                    <View style={styles.backgroundWhite}>
                                        <Image source={require('../assets/logoMudanzas.png')} />
                                    </View>
                                    <FadeInView style={styles.viewEnunciado}>
                                        <Text style={styles.textEnunciado}>Luego escojes la fecha de tu mudanza</Text>
                                        <Text style={styles.textEnunciado}>Agenda en nuestro calendario según disposición</Text>
                                        {/*<View style={styles.marginTop30}>
                                            <Button
                                                title="Ver mas"
                                                onPress={() =>
                                                    navigation.push('Inicio')}
                                            />
                                        </View>*/}
                                    </FadeInView>

                                    <FadeInView style={styles.viewEnunciado}>
                                        <Text style={styles.textEnunciado}>Recuerda agendar con buen tiempo para que no tengas ningún contratiempo con la disponibilidad</Text>
                                        <Text style={styles.textEnunciado}></Text>
                                        {/*<View style={styles.marginTop30}>
                                            <Button
                                                title="Ver mas"
                                                onPress={() =>
                                                    navigation.push('Inicio')}
                                            />
                                        </View>*/}
                                    </FadeInView>

                                    <FadeInView style={styles.viewEnunciado}>
                                        <Text style={styles.textEnunciado}>Y listo espera lo que pediste el dia y la hora seleccionadas</Text>
                                        {/*<View style={styles.marginTop30}>
                                            <Button
                                                title="Ver mas"
                                                onPress={() =>
                                                    navigation.push('Inicio')}
                                            />
                                        </View>*/}
                                    </FadeInView>

                                    <FadeInView style={styles.viewEnunciado}>
                                        <Text style={styles.textEnunciado}>Y realiza tu mudanza a tu acomodo</Text>
                                        <Text style={styles.textEnunciado}>!!Es muy sencillo¡¡</Text>
                                        <Text style={styles.textEnunciado}></Text>
                                        {/*<View style={styles.marginTop30}>
                                            <Button
                                                title="Ver mas"
                                                onPress={() =>
                                                    navigation.push('Inicio')}
                                            />
                                        </View>*/}
                                    </FadeInView>
                                    
                                </View>
                            </View>                    
                        </View>
                    </ImageBackground>
                </ScrollView>
                <FlashMessage position="bottom" />
            </View>    
        )
    }
}

const styles = StyleSheet.create({
    textEnunciado:
        {
            fontSize:20,
            color:'#ecf0f1'
        },
    viewEnunciado:
        {
            marginTop:30,
            backgroundColor:'rgba(52, 73, 94,0.4)',
            paddingHorizontal:20,
            paddingVertical:20,
            textAlign:'center'
        },
    marginTop30:
        {
            marginTop:30
        },
    width100:
        {
            width:'100%'
        },
    height:
        {
            height:'100%'
        },
    padding20:
        {
            paddingHorizontal:20,
            paddingVertical:20
        },
    backgroundWhite:
        {
            backgroundColor: 'rgba(236, 240, 241, 0.4)'
        }
  });