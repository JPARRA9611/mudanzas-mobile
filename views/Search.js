import React,{Component} from 'react'
import {GooglePlacesAutoComplete} from 'react-native-google-places-autocomplete'


export default class Search extends Component{

    render(){

        return(
            <GooglePlacesAutoComplete
                placeholder="Buscar Dirección"
                minLength={6}
                placeholderTextColor="#333"
                query={{
                    key:'AIzaSyDHadb7c4epQ61qx9D9Yt6c4vereT9nOh4',
                    language:'es'
                }}
                textInputProps={{
                    autoCapitalize:'none',
                    autoCorrect:false
                }}
                fetchDetails
                enablePoweredByContainer={false}
                styles={{
                    container:{
                        position:'absolute',
                        top: 40,
                        width:'100%'
                    },
                    textInputContainer:{
                        marginHorizontal: 10,
                        flex: 1,
                        backgroundColor: 'transparent',
                        height:54,
                        borderTopWidth:0,
                        borderBottomWidth: 0,
                    },
                    textInput:{
                        height:54,
                        margin: 0,
                        padding: 0,
                        borderRadius: 9,
                        elevation:5,
                        shadowColor: 'gray',
                        shadowOpacity: 0.1,
                        shadowOffset: {x:0,y:0},
                        shadowRadius: 15,
                        borderWidth: 1,
                        borderColor: 'gray',
                        fontSize: 18,
                    },
                    listView:{
                        marginHorizontal: 20,
                        borderWidth: 1,
                        borderColor: 'gray',
                        backgroundColor:'black',
                        elevation:5,
                        shadowColor: 'gray',
                        shadowOpacity: 0.1,
                        shadowOffset: {x:0,y:0},
                        shadowRadius: 15,
                        marginTop: 15,
                    },
                    description:{
                        fontSize: 15,
                    },
                    row:{
                        padding:18,
                        height:58
                    }
                }}
            />
        )
    }
}