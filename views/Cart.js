import React, { Component } from "react";
import { Button, View,Alert,StyleSheet,TouchableOpacity,Text,FlatList } from "react-native";
import {setCache,getCache} from "../CacheApp";
import { ListItem,Avatar } from 'react-native-elements'


export default class Carrito extends Component {

    static navigationOptions={
        title:'Mudanzas Chico'
    }

    

    constructor(props) {
        super(props);
        //var nombre=getCache('nombre');
        var nombre='Juan Sebastian';
        var nombreInciales=nombre.split(' ');
        
        this.state={
            productos:getCache('productos'),
            fecha:getCache('fecha'),
            hora:getCache('hora'),
            nombre:getCache('nombre'),
            direccion:getCache('direccion'),
            identificacion:getCache('identificacion'),
            tipoVivienda:getCache('tipoVivienda'),
            inicial:' '+nombreInciales[0].slice(0, 1)+nombreInciales[1].slice(0, 1)+' '
        }
        
    }

    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
        <ListItem
        title={item.descripcion}
        subtitle={item.valor}
        leftAvatar={{ source: { uri: item.url } }}
        bottomDivider
        chevron
        />
    )


    render(){
        const list = this.state.productos;
        return(
            <>
                <View style={{flex:1,justifyContent:'center',alignItems:'center',borderBottomWidth:1}}>
                    <Avatar
                        size="xlarge"
                        rounded
                        title={this.state.inicial}
                        onPress={() => console.log("Works!")}
                        activeOpacity={0.7}
                    />
                    <View style={{padding:20}}>
                        <Text style={{fontSize:25}}>Nombre: {this.state.nombre}</Text>
                        <Text style={{fontSize:25}}>Identificación: {this.state.identificacion}</Text>
                        <Text style={{fontSize:25}}>Dirección: {this.state.direccion}</Text>
                        <Text style={{fontSize:25}}>Casa o Apartamento: {this.state.tipoVivienda}</Text>
                        <Text style={{fontSize:25}}>Fecha: {this.state.fecha} {this.state.hora}</Text>
                    </View>
                </View>
                <View style={{flex:1}}>
                    <FlatList
                        keyExtractor={this.keyExtractor}
                        data={list}
                        renderItem={this.renderItem}
                    />
                </View>
            </>
        )
    }
}