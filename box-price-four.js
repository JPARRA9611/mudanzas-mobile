import React,{Component} from 'react';
import { View,StyleSheet,Text,Image } from 'react-native';

export default class BoxPriceFour extends Component{

    constructor(){
        super();
    }

    render(){
        const { navigation } = this.props.props;
        const imagenes=this.props.imagen.map((val,k)=>{
            return(
                <Image key={k} style={{width:'49%',height:'50%',borderColor:'#ecf0f1',borderWidth:1,margin:1,flexDirection: 'column'}} source={require('./assets/logoMudanzas.png')} />
            )
        });
        return(                 
            <View style={styles.containerView}>
                <View style={{height:'20%',borderBottomWidth:1,borderColor:'#ecf0f1',flex:1,justifyContent:'center'}}>
                    <Text style={{fontSize:20,paddingTop:20,paddingLeft:20}}>{this.props.titulo}</Text>
                </View>
                <View style={{height:'60%'}}>
                    <View style={{width:'100%',height:'80%',flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                        {imagenes}
                    </View>
                    <Text style={{height:'10%',paddingLeft:20}}>{this.props.descripcion}</Text>
                    <Text style={{height:'10%',paddingLeft:20}}>${this.props.precio}</Text>
                </View>
                <View style={{height:'20%',borderTopWidth:1,borderColor:'#ecf0f1',flex:1,justifyContent:'center'}}>
                    <Text style={{fontSize:20,paddingLeft:20}}>{this.props.titulo}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerView:
        {
            width:'90%',
            marginTop:30,
            height:400,
            marginLeft:20,
            marginRight:10,
            borderColor:'#ecf0f1',
            elevation:4,
            marginBottom:30
        }
  });