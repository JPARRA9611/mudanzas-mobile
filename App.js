import {createStackNavigator,createAppContainer} from 'react-navigation';
import Login from './Login';
import Detalle from './views/Detalle';
import Inicio from './Inicio';
import BoxPrice from './box-price';
import OtrosProductos from './views/OtrosProductos';
import Presentacion from './views/Presentacion';
import InformacionApp from './views/InformacionApp';
import Splash from './views/Splash';
import Calendar from './views/Calendar';
import Location from './views/Location';
import Cart from './views/Cart';



const MainNavigator = createStackNavigator(
  {
    Login:{screen:Login},
    Detalle:{screen:Detalle},
    Inicio:{screen:Inicio},
    BoxPrice:{screen:BoxPrice},
    OtrosProductos:{screen:OtrosProductos},
    Presentacion:{screen:Presentacion},
    InformacionApp:{screen:InformacionApp},
    Splash:{screen:Splash},
    Calendar:{screen:Calendar},
    Location:{screen:Location},
    Cart:{screen:Cart},
  },
  {
    initialRouteName:'Splash',
    defaultNavigationOptions:{
      headerStyle: {
        backgroundColor:'#3498db',
        textAlign:'center'
      },
      headerTintColor:'#fff',
      headerTitleStyle:{
          fontWeight:'bold'
      }
    }
  },
  
);

const App=createAppContainer(MainNavigator);

export default App;
