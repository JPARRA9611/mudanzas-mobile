import React from 'react';
import { View,ScrollView,Image,Text} from 'react-native';
import { Icon, Button } from 'react-native-elements';
import BoxPrice from './box-price';
import BoxPriceFour from './box-price-four';
import FlashMessage,{ showMessage, hideMessage } from "react-native-flash-message";
import {getCache,setCache} from "./CacheApp";

export default class Inicio extends React.Component{

    constructor(props){
        super(props);
        //alert(getCache('fecha'));
        setCache('productos',[]);
        
    }

    static navigationOptions = ({ navigation }) => {
            return {
            headerLeft:(
                <View style={{padding:10}}>
                    <Icon
                        name='home'
                        type='font-awesome'
                        color='white'
                        onPress={()=>navigation.navigate('Inicio')}
                    />
                </View>
            ),
            headerTitle: (
                <View style={{flex:1,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                    <View>
                        <Text style={{color:'white',fontSize:15,fontWeight:'bold'}}>Mudanzas Chico</Text>
                    </View>
                </View>
            ),
            headerRight: (
                <View style={{padding:10}}>
                    <Icon
                        name='shopping-cart'
                        type='font-awesome'
                        color='white'
                        onPress={()=>navigation.navigate('Cart')}
                    />
                </View>
            ),
        };
    };
    
    mensaje=()=>{
        showMessage({
            message: "Hello World",
            description: "This is our second message",
            type: "success",
            backgroundColor:'blue'
          });
    }
    render(){
        const { navigation } = this.props;
        return(
            <View style={{flex: 1}}>
                <FlashMessage position="bottom" />
                <ScrollView>
                    <View>
                        <View style={{height:200,width:'100%',backgroundColor:'#ecf0f1',marginBottom:10}}>
                            <Image style={{width:'100%',height:'100%'}} source={require('./assets/logoMudanzas.png')}/>
                        </View>
                        <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
                            <View >
                                <Icon
                                    raised
                                    name='truck'
                                    type='font-awesome'
                                    color='#f50'
                                    onPress={() =>
                                        navigation.push('Detalle', {
                                            itemId: Math.floor(Math.random() * 100),
                                            type:1
                                        })}
                                />

                            </View>
                            <View>
                                <Icon
                                    raised
                                    name='user'
                                    type='font-awesome'
                                    color='#f50'
                                    onPress={() =>
                                        navigation.push('Detalle', {
                                            itemId: Math.floor(Math.random() * 100),
                                            type:2
                                        })}
                                />
                            </View>
                            <View>
                                <Icon
                                    raised
                                    name='truck'
                                    type='font-awesome'
                                    color='#f50'
                                />
                            </View>
                            <View>
                                <Icon
                                    raised
                                    name='user'
                                    type='font-awesome'
                                    color='#f50'
                                />
                            </View>
                            <View>
                                <Icon
                                    raised
                                    name='truck'
                                    type='font-awesome'
                                    color='#f50'
                                />
                            </View>
                        </View>
                    </View>
                    <BoxPrice
                        titulo="Visto Recientemente"
                        imagen='./assets/logoMudanzas.png'
                        descripcion="Descripción del producto"
                        precio='15000'
                        props={this.props}
                        
                    />

                    <BoxPriceFour
                        titulo="Visto Recientemente"
                        imagen={['./assets/logoMudanzas.png','./assets/logoMudanzas.png']}
                        descripcion="Descripción del producto"
                        precio='15000'
                        props={this.props}
                    />
                </ScrollView>
            </View>
        )
    }
}