import React from 'react';
import axios from 'axios';
import { StyleSheet, Text, View, Image, Alert } from 'react-native';
import { Card,Input,Button } from 'react-native-elements';
import { Table, Row, Rows} from 'react-native-table-component';


export default class ViewCotizaciones extends React.Component{

    constructor(){
        super();
        this.state={
            tableHead:['#','Nombre','Origen','Destino'],
            tableData:[
                ['1','Sebastian Parra','Bogotá','Cali'],
                ['2','Andres Chavez','Medellin','Bogota'],
                ['3','Santiago Ruiz','Bogotá','Cali'],
            ]
        }
    }

    notificacion=(titulo,texto)=>{
        Alert.alert(
          titulo,
          texto,
          [
            {text:"Aceptar",onPress:()=>console.log('Ok')}
          ]
        );
    }

    render(){
        const state=this.state;
        console.log(state.tableData);
        return(
            <View>
                <Card title="Cotizaciones">
                    <View>
                        <Table borderStyle={{borderWidth:2,borderColor:'#c8e1ff'}}>
                            <Row data={state.tableHead} style={styles.head} textStyle={styles.text}></Row>
                            <Rows data={state.tableData} textStyle={styles.text}></Rows>
                        </Table>
                    </View>
                    <View style={{marginTop:30,textAlign:'center'}}>
                        <Button 
                            title="Volver"
                            buttonStyle={{backgroundColor:'#e74c3c'}}
                            onPress={()=>this.props.navigation.navigate('Menu')}
                        />
                    </View>
                </Card>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container: {
        flex:1,
        padding:16,
        paddingTop:30,
        backgroundColor:'#fff'
    },
    head: {
        height:40,
        backgroundColor:'#f1f8ff'
    },
    text: {
        margin:6
    }
});